import React, { Component } from "react";
import axios from "axios";
import GlobalStateStore from "./GlobalStateStore";
import { observer } from "mobx-react";
import placeholder from "../images/placeholder.png";
import InfiniteScroll from "react-infinite-scroller";

@observer
class Albums extends Component {
  constructor() {
    super();
    this.state = {
      artist_id: "",
      artist_name: "",
      albums: [],
      offset: 0,
      limit: 20,
      hasMoreItems: true,
      error: false,
    };
  }

  getSpotifyAlbumsURL = () => {
    return {
      url: `/artists/${this.state.artist_id}/albums?access_token=${GlobalStateStore.token}&offset=${this.state.offset}&limit=${this.state.limit}`,
      method: "get",
      baseURL: "https://api.spotify.com/v1/",
    };
  };

  componentDidMount() {
    const {
      match: { params },
    } = this.props;
    if (params) {
      this.setState({ artist_id: params.id });
      axios
        .get(`https://api.spotify.com/v1/artists/${params.id}?access_token=${GlobalStateStore.token}`)
        .then((response) => {
          this.setState({
            artist_name: response.data.name,
          });
          axios
            .request(this.getSpotifyAlbumsURL())
            .then((response) => {
              let albums_total = response.data.total;
              this.setState({
                albums: response.data.items,
                hasMoreItems: albums_total >= 20 ? true : false,
              });
            })
            .catch((error) => {
              this.handleErrors(error);
            });
        })
        .catch((error) => {
          this.handleErrors(error);
        });
    }
  }

  handleErrors = (error) => {
    let status = error.response.status;
    if (status === 401) {
      GlobalStateStore.progress = 0;
      localStorage.clear();
      GlobalStateStore.token = "";
      this.setState({
        error: true,
      });
    } else {
      GlobalStateStore.progress = 0;
      this.setState({
        error: true,
      });
    }
  };

  loadMoreAlbums = (n) => {
    const offset = (n - 1) * this.state.limit + 1;
    this.setState({
      offset,
      hasMoreItems: false,
    });
    axios
      .request(this.getSpotifyAlbumsURL())
      .then((response) => {
        const albums_list = response.data.items;
        if (albums_list.length) {
          this.setState({
            albums: this.state.albums.concat(albums_list),
            hasMoreItems: true,
          });
        } else {
          this.setState({
            hasMoreItems: false,
          });
        }
      })
      .catch((error) => {
        this.handleErrors(error);
      });
  };

  render() {
    let items = [];

    this.state.albums.map((album, i) => {
      return items.push(
        <div className="album" key={i}>
          <a href={album.external_urls.spotify} target="_blank" rel="noopener noreferrer" className="album_image">
            <img src={album.images.length ? album.images[0].url : placeholder} alt="Album"></img>
          </a>
          <div className="album_details">
            <a href={album.external_urls.spotify} target="_blank" rel="noopener noreferrer">
              {album.name}
            </a>
            <div className="album_artists">
              {album.artists.map((artist, i) => (
                <p key={i}>{artist.name}</p>
              ))}
            </div>
          </div>
          <div className="album_details_2">
            <p>{album.release_date}</p>
            <p>{album.total_tracks} tracks</p>
          </div>
          <a href={album.external_urls.spotify} target="_blank" rel="noopener noreferrer" className="album_peview">
            Preview on Spotify
          </a>
        </div>
      );
    });

    return (
      <div>
        {this.state.error && (
          <div className="error">
            <p>Whoops! An error occurred, please try logging again.</p>
          </div>
        )}

        {this.state.artist_id && this.state.albums.length > 0 && (
          <div>
            <div className="albums_title">
              <h1>{this.state.artist_name}</h1>
              <h2>Albums</h2>
            </div>
            <InfiniteScroll pageStart={1} loadMore={this.loadMoreAlbums} hasMore={this.state.hasMoreItems}>
              <div className="albums">{items}</div>
            </InfiniteScroll>
          </div>
        )}
      </div>
    );
  }
}
export default Albums;
