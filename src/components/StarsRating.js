import React, { Component } from "react";
import star from "../images/star.png";

class StarsRating extends Component {
  render() {
    const popularity = this.props.popularity;
    let stars = "";
    if (popularity < 20) stars = 1;
    if (popularity >= 20 && popularity < 40) stars = 2;
    if (popularity >= 40 && popularity < 60) stars = 3;
    if (popularity >= 60 && popularity < 80) stars = 4;
    if (popularity >= 80 && popularity <= 100) stars = 5;

    let html = [];

    for (let i = 0; i < stars; i++) {
      html.push(<img src={star} className="rating_star" alt="Star" key={Math.floor(Math.random() * 1000000)}></img>);
    }

    return <div className="rating_stars">{html}</div>;
  }
}
export default StarsRating;
