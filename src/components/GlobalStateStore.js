import { observable } from "mobx";

class StateStore {
  @observable token = null;
  @observable expired = false;
  @observable progress = 0;
}

let GlobalStateStore = new StateStore();

export default GlobalStateStore;
