import React, { Component } from "react";
import LoadingBar from "react-top-loading-bar";
import GlobalStateStore from "./GlobalStateStore";
import { observer } from "mobx-react";

@observer
class ProgressBar extends Component {
  onLoaderFinished = () => {
    GlobalStateStore.progress = 0;
  };

  render() {
    return <LoadingBar progress={GlobalStateStore.progress} className="loading_bar" height={3} color="red" onLoaderFinished={() => this.onLoaderFinished()} />;
  }
}

export default ProgressBar;
