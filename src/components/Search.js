import React, { Component } from "react";
import search_icon from "../images/search_icon.png";
import StarsRating from "./StarsRating";
import GlobalStateStore from "./GlobalStateStore";
import placeholder from "../images/placeholder.png";

import axios from "axios";
import { Link } from "react-router-dom";
import { observer } from "mobx-react";
import InfiniteScroll from "react-infinite-scroller";

import slugify from "../functions/slugify";

const hash = window.location.hash
  .substring(1)
  .split("&")
  .reduce(function (initial, item) {
    if (item) {
      var parts = item.split("=");
      initial[parts[0]] = decodeURIComponent(parts[1]);
    }
    return initial;
  }, {});

window.location.hash = "";

let token = hash.access_token;
let expires_at = Date.now() + Number(hash.expires_in) * 1000;
let expired;
if (token && expires_at) {
  expired = false;
} else {
  if (localStorage.getItem("token") !== null && localStorage.getItem("expires_at") !== null) {
    expired = Date.now() > localStorage.getItem("expires_at");
  } else {
    expired = false;
  }
}

if (token && expires_at) {
  GlobalStateStore.token = token;
  GlobalStateStore.expired = false;
  localStorage.setItem("token", token);
  localStorage.setItem("expires_at", expires_at);
} else if (expired) {
  GlobalStateStore.expired = true;
  localStorage.clear();
} else if (!expired && localStorage.getItem("token") && localStorage.getItem("expires_at")) {
  GlobalStateStore.expired = false;
  GlobalStateStore.token = localStorage.getItem("token");
}

@observer
class Search extends Component {
  constructor() {
    super();
    this.state = {
      q: null,
      artists: [],
      offset: 0,
      limit: 20,
      fetching: false,
      hasMoreItems: true,
      pageStart: 2,
    };
    this.searchTimeout = null;
  }

  handleErrors = (error) => {
    let status = error.response.status;
    if (status === 401) {
      GlobalStateStore.progress = 0;
      localStorage.clear();
      GlobalStateStore.token = "";
      this.setState({
        error: true,
      });
    } else {
      GlobalStateStore.progress = 0;
      this.setState({
        error: true,
      });
    }
  };

  getSpotifyURL = () => {
    return {
      url: `/search?access_token=${GlobalStateStore.token}&type=artist&q=${this.state.q}&offset=${this.state.offset}&limit=${this.state.limit}`,
      method: "get",
      baseURL: "https://api.spotify.com/v1/",
    };
  };

  searchArtists = (q) => {
    clearTimeout(this.searchTimeout);

    this.searchTimeout = setTimeout(() => {
      this.setState({
        q,
        offset: 0,
        fetching: true,
        pageStart: 2,
      });
      GlobalStateStore.progress = 100;
      if (this.state.q) {
        axios
          .request(this.getSpotifyURL())
          .then((response) => {
            let artists_list = response.data.artists.items;
            let total_artists = response.data.artists.total;
            this.setState({
              artists: artists_list,
              hasMoreItems: total_artists >= 20 ? true : false,
              fetching: false,
            });
            GlobalStateStore.progress = 0;
          })
          .catch((error) => {
            this.handleErrors(error);
          });
      } else {
        this.setState({
          artists: [],
          hasMoreItems: false,
        });
      }
    }, 500);
  };

  loadMoreArtists = () => {
    let n = this.state.pageStart;
    const offset = (n - 1) * this.state.limit + 1;
    this.setState({
      offset,
      hasMoreItems: false,
    });
    GlobalStateStore.progress = 100;
    axios
      .request(this.getSpotifyURL())
      .then((response) => {
        let artists_list = response.data.artists.items;
        this.setState({
          artists: this.state.artists.concat(artists_list),
          hasMoreItems: artists_list.length > 0 ? true : false,
          pageStart: this.state.pageStart + 1,
        });
        GlobalStateStore.progress = 0;
      })
      .catch((error) => {
        this.handleErrors(error);
      });
  };

  render() {
    let items = [];

    this.state.artists.map((item, i) => {
      return items.push(
        <div className="artist" key={i}>
          <Link to={`/artist/${item.id}/${slugify(item.name)}`} className="artist_image">
            <img src={item.images.length ? item.images[0].url : placeholder} alt="Artist"></img>
          </Link>
          <div className="artist_details">
            <Link to={`/artist/${item.id}/${slugify(item.name)}`}>{item.name}</Link>
            <p>{item.followers.total.toLocaleString()} followers</p>
            <StarsRating popularity={item.popularity}></StarsRating>
          </div>
        </div>
      );
    });

    return (
      <div>
        <div className="search">
          <input
            placeholder="Search for an artist…"
            onChange={({ target: { value } }) => this.searchArtists(value)}
            onSubmit={(e) => this.searchArtists(e)}
            className="search_field"
          ></input>
          <input type="image" alt="Submit" className="search_submit" src={search_icon}></input>
        </div>

        {!this.state.artists.length && this.state.q && !this.state.fetching && (
          <div className="error">
            <p>Whoops! No results found.</p>
          </div>
        )}

        {this.state.error && (
          <div className="error">
            <p>Whoops! An error occurred, please try logging again.</p>
          </div>
        )}

        {this.state.artists.length > 0 && (
          <InfiniteScroll
            pageStart={this.state.pageStart}
            loadMore={this.loadMoreArtists}
            hasMore={this.state.hasMoreItems}
          >
            <div className="artists">{items}</div>
          </InfiniteScroll>
        )}
      </div>
    );
  }
}
export default Search;
