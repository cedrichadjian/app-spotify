import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import GlobalStateStore from "./GlobalStateStore";

class Logout extends Component {
  state = {
    navigate: false,
  };

  handleLogout = (e) => {
    GlobalStateStore.token = "";
    localStorage.clear();
    this.setState({
      navigate: true,
    });
  };

  render() {
    if (this.state.navigate) {
      return <Redirect to="/" />;
    }
    return (
      <a onClick={this.handleLogout} href="/#">
        Logout
      </a>
    );
  }
}

export default Logout;
