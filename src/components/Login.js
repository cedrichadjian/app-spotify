import React, { Component } from "react";
import logo from "../images/spotify-logo.png";
import GlobalStateStore from "./GlobalStateStore";
import { observer } from "mobx-react";
const clientId = "d64aeac9b75e42d793e2ced9277d1ec1";
const redirectUri = "http://localhost:3000/search";

@observer
class Login extends Component {
  render() {
    return (
      <div>
        <div className="login">
          <a
            href={`https://accounts.spotify.com/authorize?client_id=${clientId}&redirect_uri=${redirectUri}&response_type=token&show_dialog=true`}
          >
            Login
            <img src={logo} alt="Spotify Logo"></img>
          </a>
        </div>
        {GlobalStateStore.expired && (
          <div className="error">
            <p>Whoops! Your token has expired, please login again.</p>
          </div>
        )}
      </div>
    );
  }
}
export default Login;
