import React, { Component } from "react";
import Logout from "./Logout";
import GlobalStateStore from "./GlobalStateStore";

class Header extends Component {
  render() {
    return (
      <header>
        <div className="title">
          <p>Spotify Artist Search</p>
          {GlobalStateStore.token && localStorage.getItem("token") !== null && <Logout></Logout>}
        </div>
      </header>
    );
  }
}
export default Header;
