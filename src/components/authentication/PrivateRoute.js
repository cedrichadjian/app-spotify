import React, { Component } from "react";
import { Route, Redirect } from "react-router-dom";
import GlobalStateStore from "../GlobalStateStore";

class PrivateRoute extends Component {
  render() {
    const { component: Component, ...props } = this.props;
    const expired = Date.now() > Number(localStorage.getItem("expires_at"));
    return (
      <Route
        {...props}
        render={(props) => {
          if (localStorage.getItem("token") !== null && !expired && GlobalStateStore.token) {
            console.log("User is logged in and the token hasn't been expired");
            return <Component {...props} />;
          } else if (localStorage.getItem("token") !== null && expired) {
            console.log("User is logged in but the token is expired.");
            return <Redirect exact to="/" />;
          } else if (localStorage.getItem("token") === null && !GlobalStateStore.token) {
            console.log("User isn't logged in.");
            return <Redirect exact to="/" />;
          }
        }}
      />
    );
  }
}

export default PrivateRoute;
