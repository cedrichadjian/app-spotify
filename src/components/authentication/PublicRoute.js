import React, { Component } from "react";
import { Route, Redirect } from "react-router-dom";
import GlobalStateStore from "../GlobalStateStore";

class PublicRoute extends Component {
  render() {
    const { component: Component, ...props } = this.props;
    const expired = Date.now() > Number(localStorage.getItem("expires_at"));
    return (
      <Route
        {...props}
        render={(props) => {
          console.log(localStorage.getItem("token") === null, !GlobalStateStore.token);
          if (localStorage.getItem("token") !== null && !expired && GlobalStateStore.token) {
            console.log("User is logged in and the token hasn't been expired");
            return <Redirect exact to="/search" />;
          } else if (localStorage.getItem("token") !== null && GlobalStateStore.token && expired) {
            console.log("User is logged in but the token is expired.");
            return <Component {...props} />;
          } else if ((localStorage.getItem("token") === null, !GlobalStateStore.token)) {
            console.log("User isn't logged in.");
            return <Component {...props} />;
          }
        }}
      />
    );
  }
}

export default PublicRoute;
