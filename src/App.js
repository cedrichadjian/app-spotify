import React, { Component } from "react";
import { BrowserRouter as Router } from "react-router-dom";

import "./App.css";
import Albums from "./components/Albums";
import Login from "./components/Login";
import Search from "./components/Search";
import Header from "./components/Header";
import ProgressBar from "./components/ProgressBar";
import PublicRoute from "./components/authentication/PublicRoute";
import PrivateRoute from "./components/authentication/PrivateRoute";

class App extends Component {
  render() {
    return (
      <main className="main">
        <ProgressBar />
        <Router>
          <Header />
          <PublicRoute exact path="/" component={Login} />
          <PrivateRoute exact path="/search" component={Search} />
          <PrivateRoute exact path="/artist/:id/:artist_name" component={Albums} />
        </Router>
      </main>
    );
  }
}
export default App;
